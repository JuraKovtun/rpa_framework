import logging
import inspect


def logger(file_name: str, logger_name: str) -> logging.Logger:
    """configure task logger
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(file_name)
    formatter = logging.Formatter(
        '%(asctime)s %(name)s %(levelname)s: %(message)s')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger


def ctx_message(message: str) -> str:
    """create an info message using the context function name
    """
    fn_name = inspect.stack()[1][3]
    return f"fn: {fn_name}, msg: '{message}'"
